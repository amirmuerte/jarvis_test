<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Controller\ResourceController;

/**
 * @Route("api/users")
 */
class UserController extends ResourceController
{
    /**
     * @Route("/", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository)
    {
        
        $query = $userRepository->findAllUsersQuery();
        return $this->success($this->paginate($query));
    }

    /**
     * @Route("/", name="user_new", methods={"POST"})
     */
    public function create(Request $request): Response
    {
        $user = new User();

        $data = $request->request->all();
        $form = $this->createForm(UserType::class, $user);
        $form->submit($data);

        try {

            if ($form->isValid()) {
            
            $this->em->persist($user);
            $this->em->flush();

            return $this->success($user);
            }
        } catch (\Exception $e) {

         
         return $this->error("error while creating user", 500);   

        }
        
        return $this->formError($form, 400);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        return $this->success($user);
    }

    /**
     * @Route("/{id}", name="user_edit", methods={"PATCH"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $data = $request->request->all();

        $form->submit($data, false);

        if ($form->isValid()) {
            $this->logger->info("some user has been updated");
            $this->logger->info(json_encode($data));
            $this->em->flush();
            return $this->success($user);
        }

        return $this->formError($form);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        try {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();

            return $this->success("user deleted");
        } catch (\Exception $e) {

             return $this->error("error while deleting user", 500);
        }
       
    }
}
