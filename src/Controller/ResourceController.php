<?php

namespace App\Controller;


use Doctrine\ORM\EntityManagerInterface;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use App\Entity\User;


class ResourceController extends AbstractFOSRestController
{

    
    /** @var EntityManagerInterface */
    protected $em;
    
    /** @var LoggerInterface */
    protected $logger;
    

    public function __construct(EntityManagerInterface $em, $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }
    
    protected function view($data = null, $statusCode = null, $additionalScopes = [], $checkScopes = true)
    {
        $view = parent::view($data, $statusCode);
        
        if ($checkScopes) {
            $rawScopes = Request::createFromGlobals()->query->get('scopes');
            $scopes = $rawScopes ? explode(',', $rawScopes) : [];
            
            $scopes[] = "Default";
            
            $scopes = array_merge($scopes, $additionalScopes);
            
            $context = new Context();
            $context->setGroups($scopes);
            
            $view->setContext($context);
        }

        return $view;
    }
    
    protected function success($data, $statusCode = 200, $additionalScopes = [], $checkScopes = true)
    {
        return $this->handleView($this->view($data, $statusCode, $additionalScopes, $checkScopes));
    }

    /**
     * paginate function
     * @param QueryBuilder $queryBuilder from repository
     * @param integer $page the page the client want
     * @param integer $limit the maximum number of items per page
     * @return array containing the items, the count or number of elements in items, and the total which is the total number of items in the database.
     */
    protected function paginate($queryBuilder, $page = null, $limit = null)
    {
        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        
        $request = Request::createFromGlobals();

        if (!$page) {
            $page = $request->query->get('page', 1);
        }

        if (!$limit) {
            $limit = $request->query->get('limit', 10);
        }
        $pagerfanta->setMaxPerPage($limit);
        $pagerfanta->setCurrentPage($page);

        $arrayResult = [];
        $results = $pagerfanta->getCurrentPageResults();

        foreach ($results as $result) {
            $arrayResult[] = $result;
        }

        //better pagination
        return [
            'items' => $arrayResult,
            'count' => count($arrayResult),
            'total' => $pagerfanta->getNbResults(),
            'nbPages' => $pagerfanta->getNbPages(),
        ];
    }

    protected function error($data, $statusCode = 400)
    {
        $response = [
            'code' => $statusCode,
            'errors' => $data,
        ];
        
        return $this->handleView($this->view($response, $statusCode, [], false));
    }
    
    protected function fatal($data, $statusCode = 500)
    {
        $response = [
            'code' => $statusCode,
            'errors' => $data,
        ];
        $this->logError($data);
        
        return $this->error($response, $statusCode);
    }
    
    protected function logError($msg, $extraData = [])
    {
        $this->logger->error($msg, $extraData);
    }
    
   
    
    protected function formError(FormInterface $form, $statusCode = 400)
    {
        $errors = $form->getErrors();
        
        $response = [
            'code' => $statusCode,
            'errors' => $this->getFormErrors($form),
        ];

        return $this->handleView($this->view($response, $statusCode));
    }
    
    private function getFormErrors(FormInterface $form)
    {
        $errors = [];
        
        if (!$form->count()) {
            foreach ($form->getErrors() as $error) {
                $errors[] = $error->getMessage();
            }
        } else {
            foreach ($form->getErrors() as $error) {
                $errors['main'][] = $error->getMessage();
            }

            foreach ($form->all() as $child) {
                $childErrors = $this->getFormErrors($child);

                if ($childErrors) {
                    $errors[$child->getName()] = $childErrors;
                }
            }
        }

        return $errors ?: null;
    }

}
